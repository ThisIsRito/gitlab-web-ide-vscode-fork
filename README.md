# DEPRECATED

MOVED TO https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/

# GitLab Web IDE - VSCode Fork

- [Contributing](./GL_CONTRIBUTING.md)
- [Fork Changelog](./GL_CHANGELOG.md)
- [About forking process](./GL_README_FORK_PROCESS.md)

## What is this?

This is a fork of the [vscode project](https://github.com/microsoft/vscode), used to power [GitLab's Web IDE](https://gitlab.com/gitlab-org/gitlab-web-ide/).

This fork contains some minimal changes required to tailor the VSCode editing experience toward the GitLab Web IDE use cases. It also provides a consumable build for the [Web IDE package](https://gitlab.com/gitlab-org/gitlab-web-ide/) to consume.

## Getting started

Please see the [contributing guidelines](./GL_CONTRIBUTING.md) for information on making changes, building, and running this project.

## Legal

This project is not affiliated with Microsoft Corporation.
