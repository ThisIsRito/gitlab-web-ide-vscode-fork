# Contributing to GitLab Web IDE - VSCode Fork

## Goals

- Minimize changes made to this fork.
- Make it **very** clear **the location** and **reason** for any change made ontop of the base [vscode project](https://github.com/microsoft/vscode).

## Rules

- Any new files **must** be prefixed with `gl_` or `GL_`, with the exception of files that require a specific name (e.g. `README.md` or `.gitlab-ci.yml`).
- Changes made to pre-existing files must be surrounded in commented regions, starting with `gl_mod START` and ending with `gl_mod END`. Please include a reason for the change with links for more context. For example:

  ```patch
   function vscodeFoo(a, b) {
     const c = a.ref || 'DEFAULT';

     return {
       c,
  -    disabled: false,
  +    // gl_mod START
  +    // We need this `disabled` flag to be true, otherwise
  +    // users are able to accidentally break everything.
  +    // See issue: https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/12
  +    disabled: true
  +    // gl_mod END
     };
   }
  ```

- Every cohesive change must be documented in `GL_CHANGELOG.md`. Please include any links to issues, comments, or merge requests which will provide more context.

## Build

For more context, also see the [build instructions on VSCode's wiki](https://github.com/microsoft/vscode/wiki/How-to-Contribute#build)

### Prerequisites

- [NodeJS](https://nodejs.org/en/), version >=16.14.x and <17
- [Yarn](https://classic.yarnpkg.com/en/docs/install)

### Running the build process

For GitLab's Web IDE, we are only interested in the web build.

```
yarn gulp core-ci
yarn gulp vscode-web-min-ci
```

## Versioning and releasing

eere's an example of a version for the artifacts deployed from this project:

```
1.69.1-0.0.1-dev-20220722150450
```

- `1.69.1` is the base VSCode version
- `0.0.1` is the version of this fork
- `dev` is a flag that the release is meant only for development purposes.
  This could be replaced with `rc` if the artifact was a release candidate.
- `20220722150450` is the timestamp of the artifacts creation

We'll use GitLab releases to host these release artifacts and create tags based
on the version name of the artifact created in the `main` branch.

**Please note:** Official releases will not have `rc`, `dev`, or timestamp extension.
