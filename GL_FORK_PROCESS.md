# Goals

- Reproducible historic builds
	- Builds need to come from "tags" and release process includes tagging
- Intent of changes
  - Each individual commit will represent a cohesive, atomic change to the  
    upstream behavior.
	- History of commits is really important!!
	- Let's include a CHANGELOG for every MR
- Reduce the cognitive load of knowing what changes we are making in our
  fork.  This goal is _intentionally_ prioritized over preserving
  the historic state of commits on the fork, which is why we are
  preferring the `rebase ... -onto ...` paradigm over a merge-based
  paradigm. In other words, this fork is treated just like a feature
  branch.  Don't fear the `rebase`.
- See some of the arguments made here for this philosophy:
  https://github.com/thewoolleyman/gitrflow/blob/master/readme/goals_and_philosophy.md
  - TL;DR previous versions of history don't matter, a clean current state
  with atomic commits showing cohesive intent is most important.  Just like
  any other feature branch.

Downsides of this approach:

- Other than inspecting the history of previous tags, there's
  no way to see the previous versions of history by looking
  at the history of the main branch. But as described above,
  this is an intentional tradeoff to lower the cognitive load
  of understanding individual atomic changes.

## Process

TODO: expand with more details

- Find the newer upstream tag we want
- Open an MR
- Rebase the current fork commits onto the new tag
- Make any necessary changes, fully document them
- Interactive rebase changes into the relevant previous fork commits, if they
  are updates to the same changed behavior 
- Push, get CI green, run tests, exploratory test
- Make sure that main branch has a tag of the latest commit
- force push the state of the MR to main
- rebase any open MR branches onto the new main

# Notes on Merge Requests 

- The process for updating this repo against a new upstream tag involves rewriting/rebasing
  the history of main.
- This means that any MR which is open while the main branch is rebased will need
  to be rebased onto the new upstream.
- This will be a command like:  

    ```
    # ensure `main` branch is pulled with latest 
    git rebase (previous `git merge-base` SHA) --onto main
    ```


# Updating from upstream

TBD

- `git remote add upstream https://github.com/microsoft/vscode.git`
- `git fetch upstream`

To initialize

- `git reset --hard ${NAME_OF_UPSTREAM_TAG}`


